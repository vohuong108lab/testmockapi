import './App.css';
import Get from './components/Get';
import Post from './components/Post';
import Put from './components/Put';
import Delete from './components/Delete';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
            <Route path="/add">
              <Post />
            </Route>
            <Route path="/delete">
              <Delete />
            </Route>
            <Route path="/update">
              <Put />
            </Route>
            <Route path="/">
              <Get />
            </Route>
        </Switch>
        
      </Router>
      
    </div>
  );
}

export default App;
