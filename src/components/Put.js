import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useForm } from "react-hook-form"
import "./Styles.css"

const Put = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = async (data) => {
        const user = {
            id: data.id,
            userName: data.userName,
            age: data.age
        
        };
        const url = `http://localhost:3000/posts/${user.id}`;
        const res = await axios.put(url, { ...user });

        console.log(res.data);
        console.log(data)
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <label for="id" >User ID</label>
                <input id="id" {...register("id", { required: true })} />
                <label for="userName" >User Name Update</label>
                <input id="userName" {...register("userName", { required: true })} />
                <label for="age" >User Age Update</label>
                <input id="age" {...register("age", { required: true })} />
                {/* errors will return when field validation fails  */}
                {errors.exampleRequired && <span>This field is required</span>}
                
                <button type="submit" >UPDATE</button>
            </form>
        </>
    )
}

export default Put;
