import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";

const useStyles = makeStyles({
    btn: {
        display: "inline-block",
        margin: 10
    },

    container: {
        width: 650,
        marginTop: 50,
        marginBottom: 10,
        marginLeft: "auto",
        marginRight: "auto"
    }
});

const Get = () => {
    const [data, setdata] = useState([]);
    const classes = useStyles();

    useEffect(() => {
        const getData = async () => {
            const res = await axios.get('http://localhost:3000/posts');
            
            const data = res.data.sort((a, b) => {
                return Number(a.id) - Number(b.id)
            });
            setdata(data);
            
        }

        getData();
        
    }, [])

    return (
        <div className={classes.container}>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>User ID</TableCell>
                        <TableCell align="center">User Name</TableCell>
                        <TableCell align="center">Age</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {data.map((user) => (
                        <TableRow key={user.id}>
                            <TableCell component="th" scope="row">
                                {user.id}
                            </TableCell>
                            <TableCell align="center">{user.userName}</TableCell>
                            <TableCell align="center">{user.age}</TableCell>
                    
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            </TableContainer>
            <div className={classes.btn}>
                <Button component={Link} to="/add" variant="contained" color="primary">Add User</Button>
            </div>
            <div className={classes.btn}>
                <Button component={Link} to="/delete" variant="contained" color="secondary">Delete User</Button>
            </div>
            <div className={classes.btn}>
                <Button component={Link} to="/update" variant="contained" color="primary">Update User</Button>
            </div>
            
            
            
        </div>
    )
}

export default Get
