import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useForm } from "react-hook-form"
import "./Styles.css"

const Delete = () => {
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = async (data) => {
        const url = `http://localhost:3000/posts/${data.id}`;
        const res = await axios.delete(url);
        console.log(data);
        console.log(res);
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <label for="id" >User ID</label>
                <input id="id" {...register("id", { required: true })} />
                {/* errors will return when field validation fails  */}
                {errors.exampleRequired && <span>This field is required</span>}
                
                <button type="submit" >DELETE</button>
            </form>
        </>
    )
}

export default Delete;
